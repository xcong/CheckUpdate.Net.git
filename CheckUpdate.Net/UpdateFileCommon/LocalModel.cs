﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UpdateFileCommon
{
    public class LocalModel
    {
        public string ServerURL { get; set; }
        public string LocalVersion { get; set; }
        public int VersionValue { get; set; }
    }
}
